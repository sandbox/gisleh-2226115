INF5272 README.txt
==================


CONTENTS OF THIS FILE
---------------------

* Introduction
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

This project contains example modules created for the course INF5272
at the University of Oslo.

* For a full description of the project, visit the project page:
  https://drupal.org/sandbox/gisleh/2226115

* To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/2226115


INSTALLATION
------------

1. Extract the project directory into the directory where you keep
   contributed modules (e.g. sites/all/modules/).

2. Enable the module and submodules you want to check out on the
   Modules list page in the administrative interface.

3. Clear all caches.


CONFIGURATION
-------------

The parent module has no menu or modifiable settings.  See the
README.txt of submodules for specifics for these.


MAINTAINERS
-----------

Gisle Hannemyr <gisle@hannemyr.no> is the current maintainer.
